import React, {Component, Suspense} from 'react'
import { BrowserRouter as Router, Route, Switch, Redirect} from "react-router-dom";
import ScrollToTop from "Components/Others/ScrollToTop"
import {
    Login,
    Dashboard,
    Usuarios,
    Cursos,
    Docentes,
    Alumnos
} from "Views/Admin"
import {
    AdminHeader,
    AdminSidebar
} from "Layout/Admin"
import {
    Inicio
} from "Views/Front"

export default class MyRouter extends Component {
    render() {
        return (
            <Router>
                <ScrollToTop>
                    <div className="wrapper">
                        <Suspense fallback={<span>Cargando...</span>}>
                        <Switch>
                            <Route path="/admin">
                                <div className="page-admin">
                                    <AdminHeader />
                                    <AdminSidebar />
                                    <Route path="/admin" exact component={Dashboard} />
                                    <Route path="/admin/usuarios" exact component={Usuarios} />
                                    <Route path="/admin/cursos" exact component={Cursos} />
                                    <Route path="/admin/docentes" exact component={Docentes} />
                                    <Route path="/admin/alumnos" exact component={Alumnos} />
                                </div>
                            </Route>
                            <Route path="/login">
                                <div className="login-page">
                                    <Route path="/login" exact component={Login} />
                                </div>
                            </Route>
                            <Route path="*">
                                <div>No encontrado</div>
                            </Route>
                            <Route path="/">
                                <div className="page-inicio">
                                    <Route path="/" exact component={Inicio} />
                                </div>
                            </Route>
                            
                        </Switch>
                        </Suspense>
                    </div>
                </ScrollToTop>
            </Router>
        )
    }
}
