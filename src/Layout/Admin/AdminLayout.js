import React, { Component } from 'react'
import { Link } from "react-router-dom";

export class AdminLayout extends Component {
    componentWillMount = () =>{
        document.title = this.props.title
    }
    render() {
        return (
            <div className={"content-wrapper " + this.props.classPage}>
                <section className="content-header">
                    <div className="container-fluid">
                        <div className="row mb-2">
                        <div className="col-sm-6">
                            <h1>{this.props.titlepage}</h1>
                        </div>
                        <div className="col-sm-6">
                            <ol className="breadcrumb float-sm-right">
                                <li className="breadcrumb-item"><Link to={"/admin"}>Inicio</Link></li>
                            <li className="breadcrumb-item active">{this.props.titlepage}</li>
                            </ol>
                        </div>
                        </div>
                    </div>
                </section>
                <section class="content">
                    {
                        this.props.children
                    }
                </section>
            </div>
        )
    }
}
