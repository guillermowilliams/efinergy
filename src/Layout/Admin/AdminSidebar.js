import React, { Component } from 'react'
import { NavLink } from "react-router-dom";

export class AdminSidebar extends Component {
    render() {
        return (
            <aside className="main-sidebar sidebar-dark-primary elevation-4">
                <a href="../../index3.html" className="brand-link">
                    <img src={require("Assets/img/AdminLTELogo.png")}
                        alt="AdminLTE Logo"
                        className="brand-image img-circle elevation-3" />
                    <span className="brand-text font-weight-light">AdminLTE 3</span>
                </a>

                <div className="sidebar">
                    <div className="user-panel mt-3 pb-3 mb-3 d-flex">
                        <div className="image">
                            <img src={require("Assets/img/user2-160x160.jpg")} className="img-circle elevation-2" alt="User Image" />
                        </div>
                        <div className="info">
                            <a href="#" className="d-block">Alexander Pierce</a>
                        </div>
                    </div>

                    <nav className="mt-2">
                        <ul className="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                            <li className="nav-item has-treeview">
                                <a href="#" className="nav-link">
                                    <i className="nav-icon fas fa-tachometer-alt"></i>
                                    <p>
                                        Admin Web
                                        <i className="right fas fa-angle-left"></i>
                                    </p>
                                </a>
                                <ul className="nav nav-treeview">
                                    <li className="nav-item">
                                        <a href="../../index.html" className="nav-link">
                                            <i className="far fa-circle nav-icon"></i>
                                            <p>Index</p>
                                        </a>
                                    </li>
                                    <li className="nav-item">
                                        <a href="../../index2.html" className="nav-link">
                                            <i className="far fa-circle nav-icon"></i>
                                            <p>Nosotros</p>
                                        </a>
                                    </li>
                                    <li className="nav-item">
                                        <a href="../../index3.html" className="nav-link">
                                            <i className="far fa-circle nav-icon"></i>
                                            <p>Proyectos</p>
                                        </a>
                                    </li>
                                    <li className="nav-item">
                                        <a href="../../index.html" className="nav-link">
                                            <i className="far fa-circle nav-icon"></i>
                                            <p>Eventos</p>
                                        </a>
                                    </li>
                                    <li className="nav-item">
                                        <a href="../../index2.html" className="nav-link">
                                            <i className="far fa-circle nav-icon"></i>
                                            <p>Blog</p>
                                        </a>
                                    </li>
                                    <li className="nav-item">
                                        <a href="../../index3.html" className="nav-link">
                                            <i className="far fa-circle nav-icon"></i>
                                            <p>Contacto</p>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li className="nav-item">
                                <a href="../gallery.html" className="nav-link">
                                    <i className="nav-icon fas fa-chart-bar"></i>
                                    <p>
                                        Estadisticas
                                    </p>
                                </a>
                            </li>
                            <li className="nav-item">
                                <NavLink to={"/admin/usuarios"} className="nav-link">
                                    <i className="nav-icon fas fa-user"></i>
                                    <p>
                                        Usuarios
                                    </p>
                                </NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink to={"/admin/cursos"} className="nav-link">
                                    <i className="nav-icon fas fa-book"></i>
                                    <p>
                                        Cursos
                                    </p>
                                </NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink to={"/admin/docentes"} className="nav-link">
                                    <i className="nav-icon fas fa-briefcase"></i>
                                    <p>
                                        Docentes
                                    </p>
                                </NavLink>
                            </li>
                            <li className="nav-item">
                                <a href="../gallery.html" className="nav-link">
                                    <i className="nav-icon fas fa-cogs"></i>
                                    <p>
                                        Grupos
                                    </p>
                                </a>
                            </li>
                            <li className="nav-item">
                                <NavLink to={"/admin/alumnos"} className="nav-link">
                                    <i className="nav-icon fas fa-users"></i>
                                    <p>
                                        Alumnos
                                    </p>
                                </NavLink>
                            </li>
                            <li className="nav-item has-treeview">
                                <a href="#" className="nav-link">
                                    <i className="nav-icon fas fa-table"></i>
                                    <p>
                                        Intranet
                                        <i className="right fas fa-angle-left"></i>
                                    </p>
                                </a>
                                <ul className="nav nav-treeview">
                                    <li className="nav-item">
                                        <a href="../../index.html" className="nav-link">
                                            <i className="far fa-circle nav-icon"></i>
                                            <p>Alumno</p>
                                        </a>
                                    </li>
                                    <li className="nav-item">
                                        <a href="../../index2.html" className="nav-link">
                                            <i className="far fa-circle nav-icon"></i>
                                            <p>Horario</p>
                                        </a>
                                    </li>
                                    <li className="nav-item">
                                        <a href="../../index3.html" className="nav-link">
                                            <i className="far fa-circle nav-icon"></i>
                                            <p>Docente</p>
                                        </a>
                                    </li>
                                    <li className="nav-item">
                                        <a href="../../index.html" className="nav-link">
                                            <i className="far fa-circle nav-icon"></i>
                                            <p>Calendario</p>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>
            </aside>
        )
    }
}
