import React from 'react';
import ReactDOM from 'react-dom';
import MyRouter from 'Router/MyRouter';
import $ from "jquery"
import json from "Components/Others/DataTableSpanish.json"
import DataTable from "datatables.net-bs4"
import '@fortawesome/fontawesome-free/css/all.min.css'
import 'sweetalert2/src/sweetalert2.scss'
import 'bootstrap/dist/css/bootstrap.min.css';
import 'Assets/css/index.scss';
import * as serviceWorker from './serviceWorker';

window.$ = $
window.json = json

ReactDOM.render(<MyRouter />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
