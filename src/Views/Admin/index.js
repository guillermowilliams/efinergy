import {lazy} from "react"
export const Login= lazy(()=> import(/* webpackChunkName: "login" */"./Login"))
export const Dashboard= lazy(()=> import(/* webpackChunkName: "dashboard" */"./Dashboard"))
export const Usuarios= lazy(()=> import(/* webpackChunkName: "usuarios" */"./Usuarios"))
export const Cursos= lazy(()=> import(/* webpackChunkName: "cursos" */"./Cursos"))
export const Docentes= lazy(()=> import(/* webpackChunkName: "docentes" */"./Docentes"))
export const Alumnos= lazy(()=> import(/* webpackChunkName: "alumnos" */"./Alumnos"))