import React, { Component } from 'react'
import {Redirect} from 'react-router-dom';
import {PostData} from 'Services/PostData';
import { AdminLayout } from "Layout/Admin"

export default class Dashboard extends Component {
    constructor(props){
        super(props)

        this.state = {
            data: [],
            redirectToReferrer: false,
            name: ''
        }
    }

    componentWillMount = () =>{
        if (localStorage.getItem("userData")) {
            console.log('logeado')
        }else{
            this.setState({ redirectToReferrer: true })
        }
    }

    render() {
        if (this.state.redirectToReferrer) {
            return (<Redirect to={'/login'}/>)
        }
        return (
            <AdminLayout title={"Efinergy - Dashboard"} classPage={"page-dashboard"} titlepage={"Dashboard"}>
                Este es mi dashboard
            </AdminLayout>
        )
    }
}
