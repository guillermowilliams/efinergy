import React, { Component } from 'react'
import {Redirect} from 'react-router-dom';
import {PostData} from 'Services/PostData';

export default class Login extends Component {
    constructor(props){
        super(props);
        this.state = {
            username: '',
            password: '',
            redirectToReferrer: false
        }
        this.login = this.login.bind(this);
        this.onChange = this.onChange.bind(this);
    }

    login = () => {
        if(this.state.username && this.state.password){
            PostData('login',this.state).then((result) => {
                let responseJson = result;
                console.log(responseJson)
                if(responseJson.userData){
                    localStorage.setItem('userData',JSON.stringify(responseJson));
                    this.setState({redirectToReferrer: true});
                }
            });
        }

        //SignUp
        // if(this.state.username && this.state.password && this.state.email && this.state.name){
        //     PostData('signup',this.state).then((result) => {
        //         let responseJson = result;
        //         if(responseJson.userData){         
        //             sessionStorage.setItem('userData',JSON.stringify(responseJson));
        //             this.setState({redirectToReferrer: true});
        //         }
        //     });
        // }
    }

    onChange = (e) => {
        this.setState({ [e.target.name]:e.target.value })
    }

    onEnter = (e) => {
        if (e.which == 13) {
            this.login()
        }
    }

    render() {
        if (this.state.redirectToReferrer || localStorage.getItem('userData')){
            return (<Redirect to={'/admin'}/>)
        }
        return (
            <div className="login-box">
                <div className="login-logo">
                    <a href="../../index2.html"><b>Admin</b>LTE</a>
                </div>
                <div className="card">
                    <div className="card-body login-card-body">
                        <p className="login-box-msg">Iniciar sesión</p>

                            <div className="input-group mb-3">
                                <input className="form-control" type="text" name="username" placeholder="Usuario" value={this.state.username} onChange={(e) => this.onChange(e)} onKeyPress={(e) => this.onEnter(e)} />
                                <div className="input-group-append">
                                    <div className="input-group-text">
                                        <span className="fas fa-envelope"></span>
                                    </div>
                                </div>
                            </div>
                            <div className="input-group mb-3">
                                <input className="form-control" type="password" name="password" placeholder="Contraseña" value={this.state.password} onChange={(e) => this.onChange(e)} onKeyPress={(e) => this.onEnter(e)} />
                                <div className="input-group-append">
                                    <div className="input-group-text">
                                        <span className="fas fa-lock"></span>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-12">
                                    <button type="submit" className="btn btn-primary btn-block">Ingresar</button>
                                </div>
                            </div>
                            
                    </div>
                </div>
            </div>
        )
    }
}
