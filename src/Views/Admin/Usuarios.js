import React, { Component } from 'react'
import { AdminLayout } from "Layout/Admin"

const json = window.json;
const $ = window.$;

export default class Usuarios extends Component {
    componentDidMount = () =>{
        this.fnInitializeTable()
    }

    fnInitializeTable = () =>{ 
        let thes = this;
        let configTables = {
            language:json,
            order:[[0,"asc"]],
            columnDefs: [ 
                {
                    'orderable': false,
                },
                { responsivePriority: 1, targets: 0 },
            ],
            dom:'<"toolbar">frtip',
            responsive: true
        }
        $.fn.DataTable.ext.pager.numbers_length = 5;
        let table = $('.dataTable').DataTable(configTables)

        $("div.toolbar").html(`
        <div class="content-toolbar d-flex align-items-center">
            <div class="form-group">
                <input type="text" class="form-control shadow-sm pr-5" placeholder="Codigo">
            </div>
            <div class="form-group ml-2">
                <input type="text" class="form-control shadow-sm pr-5" placeholder="Nombre">
            </div>
            <div class="form-group ml-2">
                <select class="form-control shadow-sm pr-5">
                    <option value="">Cargo</option>
                </select>
            </div>
            <div class="form-group ml-2">
                <select class="form-control shadow-sm pr-5">
                    <option value="">Estado</option>
                </select>
            </div>
        </div>
        `);
    }

    render() {
        return (
            <AdminLayout title={"Efinergy - usuarios"} classPage={"page-users"} titlepage={"Usuarios"}>
                <div class="card">
                    <div class="card-header d-flex justify-content-between align-items-center w-100">
                        <h3 class="card-title">Tablero De Datos</h3>

                        <div class="card-tools">
                            <button className="btn btn-success btn-sm"><i className="fas fa-plus" /> Agregar usuario</button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div className="table-responsive">
                            <table className="table table-bordered table-sm dataTable">
                                <thead>
                                    <tr>
                                        <th>Codigo</th>
                                        <th>Nombre</th>
                                        <th>Núm. doc.</th>
                                        <th>Fecha nac.</th>
                                        <th>Cargo</th>
                                        <th>Correo</th>
                                        <th>Teléfono</th>
                                        <th>Estado</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>000001</td>
                                        <td>Juica Ruidias Guillermo Williams</td>
                                        <td>DNI 72682726</td>
                                        <td>22/05/1994</td>
                                        <td>Administrador</td>
                                        <td>guillermojuica3@gmail.com</td>
                                        <td>946591615</td>
                                        <td>Activo</td>
                                        <td>
                                            <button className="btn btn-sm btn-success"><i className="fas fa-pen"></i></button>
                                            <button className="btn btn-sm btn-danger"><i className="fas fa-trash"></i></button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </AdminLayout>
        )
    }
}
